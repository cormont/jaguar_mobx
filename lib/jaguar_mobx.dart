library jaguar_mobx;

import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:mobx/mobx.dart';

mixin ObservableListProcessor<T> implements FieldProcessor<ObservableList<T>, List<T>> {
  @override
  ObservableList<T> deserialize(List<T> value) {
    if (value == null) return null;
    return ObservableList.of(value);
  }

  @override
  List<T> serialize(ObservableList<T> value) {
    if (value == null) return null;
    return value.toList();
  }
}
