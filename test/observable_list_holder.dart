import 'package:equatable/equatable.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:mobx/mobx.dart';

import 'package:jaguar_mobx/jaguar_mobx.dart';

part 'observable_list_holder.jser.dart';

class ObservableListHolder with EquatableMixin {
  ObservableList<int> field;

  ObservableListHolder(this.field);

  @override
  List<Object> get props => [field];
}

class ObservableListSerializer with ObservableListProcessor<int> {
  const ObservableListSerializer();
}

@GenSerializer(fields: const {
  'field': const EnDecode(processor: const ObservableListSerializer(), isNullable: true),
})
class ObservableListHolderSerializer extends Serializer<ObservableListHolder> with _$ObservableListHolderSerializer {}
