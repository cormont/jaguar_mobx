// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'observable_list_holder.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$ObservableListHolderSerializer
    implements Serializer<ObservableListHolder> {
  final _observableListSerializer = const ObservableListSerializer();
  @override
  Map<String, dynamic> toMap(ObservableListHolder model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'props', passProcessor.serialize(model.props));
    setMapValue(ret, 'field', _observableListSerializer.serialize(model.field));
    return ret;
  }

  @override
  ObservableListHolder fromMap(Map map) {
    if (map == null) return null;
    final obj = ObservableListHolder(getJserDefault('field'));
    obj.field = _observableListSerializer.deserialize(map['field']);
    return obj;
  }
}
