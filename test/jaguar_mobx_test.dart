import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:mobx/mobx.dart';
import 'package:test/test.dart';

import 'observable_list_holder.dart';

void main() {
  test('can cycle observable list through serialization process', () {
    final serializer = ObservableListHolderSerializer();
    final repo = new JsonRepo()..add(serializer);

    var obj = ObservableListHolder(ObservableList.of([1, 2, 3]));
    expect(cycle(repo, obj), equals(obj));

    obj = ObservableListHolder(ObservableList.of([]));
    expect(cycle(repo, obj), equals(obj));

    obj = ObservableListHolder(null);
    expect(cycle(repo, obj), equals(obj));
  });
}

T cycle<T>(JsonRepo repo, T obj) {
  return repo.from<T>(repo.to(obj));
}
